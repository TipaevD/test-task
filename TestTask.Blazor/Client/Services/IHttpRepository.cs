﻿using System.Net.Http;
using System.Threading.Tasks;

namespace TestTask.Blazor.Client.Services
{
    public interface IHttpRepository
    {
        Task<TResponse> PostRequestAsync<TResponse>(string route, object body);
    }
}
