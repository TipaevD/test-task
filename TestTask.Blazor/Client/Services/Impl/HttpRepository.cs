﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;

namespace TestTask.Blazor.Client.Services.Impl
{
    public class HttpRepository : IHttpRepository
    {
        private readonly HttpClient _client;
        private readonly JsonSerializerOptions _jsonOptions;

        public HttpRepository(HttpClient client)
        {
            _client = client;

            _jsonOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
        }

        public async Task<TResponse> PostRequestAsync<TResponse>(string route, object body)
        {
            var response = await _client.PostAsJsonAsync(route, body, _jsonOptions);

            return await response.Content.ReadFromJsonAsync<TResponse>(_jsonOptions);
        }
    }
}
