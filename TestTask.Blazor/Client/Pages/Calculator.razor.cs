﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Blazor.Client.Domain;
using TestTask.Blazor.Client.Services;

namespace TestTask.Blazor.Client.Pages
{
    public partial class Calculator
    {
        [Inject]
        public IHttpRepository HttpRepository { get; set; }

        private double _calculationResult = 0;

        private string _inputValue = string.Empty;

        private List<HistoryDto> _historyList = new List<HistoryDto>();

        public async Task CalculateAsync()
        {
            var values = string.IsNullOrEmpty(_inputValue) ? 
                new List<double> { } : _inputValue.Split(',').Select(double.Parse).ToList();

            var result = await HttpRepository.PostRequestAsync<double>("calculation/Calculate", new
            {
                Values = values,
            });

            _calculationResult = result;

            _historyList.Add(new HistoryDto
            {
                Request = _inputValue,
                RequestTime = DateTimeOffset.Now,
                Result = result,
            });

            StateHasChanged();
        }
    }
}
