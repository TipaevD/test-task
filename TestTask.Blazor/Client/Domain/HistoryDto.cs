﻿using System;

namespace TestTask.Blazor.Client.Domain
{
    public class HistoryDto
    {
        public string Request { get; set; }

        public DateTimeOffset RequestTime { get; set; }

        public double Result { get; set; }
    }
}
