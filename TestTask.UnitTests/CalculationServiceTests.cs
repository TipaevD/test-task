using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestTask.Main.Services;
using TestTask.Main.Services.Impl;

namespace TestTask.UnitTests
{
    public class CalculationServiceTests
    {
        private readonly Mock<ICalculationSettings> _settingsMock;
        private readonly IMemoryCache _cache;

        public CalculationServiceTests()
        {
            _settingsMock = new Mock<ICalculationSettings>();

            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();
            _cache = serviceProvider.GetService<IMemoryCache>();

            _settingsMock.Setup(x => x.MinimumValue).Returns(1.0);
            _settingsMock.Setup(x => x.MaximumValue).Returns(50.0);
            _settingsMock.Setup(x => x.Limit).Returns(5);
            _settingsMock.Setup(x => x.MinDelayTime).Returns(100);
            _settingsMock.Setup(x => x.MaxDelayTime).Returns(1000);
        }

        [Fact]
        public async Task CalculateSum_CorrectArgments_CorrectResults()
        {
            var values = new List<double> { 1, 2, 3, 1 };

            var service = new CalculationService(_settingsMock.Object, _cache);

            var result = await service.CalculateSum(values);

            Assert.Equal(15, result);
        }

        [Fact]
        public async Task CalculateSum_EmptyValues_ThrowArgumentException()
        {
            var values = new List<double> { };

            var service = new CalculationService(_settingsMock.Object, _cache);

            var exception = await Assert.ThrowsAsync<ArgumentException>(() => service.CalculateSum(values));

            Assert.NotNull(exception);
            Assert.Equal("�������� ������ �� ����� ��������.", exception.Message);
        }

        [Fact]
        public async Task CalculateSum_IncorrectCountOfValues_ThrowArgumentException()
        {
            var values = new List<double> { 1, 2, 3, 4, 5, 6};

            var service = new CalculationService(_settingsMock.Object, _cache);

            var exception = await Assert.ThrowsAsync<ArgumentException>(() => service.CalculateSum(values));

            Assert.NotNull(exception);
            Assert.Equal("���������� �������� �������� ��������� ������������� ����� - 5.", exception.Message);
        }

        [Fact]
        public async Task CalculateSum_IncorrectMaxValue_ThrowArgumentException()
        {
            var values = new List<double> { 54 };

            var service = new CalculationService(_settingsMock.Object, _cache);

            var exception = await Assert.ThrowsAsync<ArgumentException>(() => service.CalculateSum(values));

            Assert.NotNull(exception);
            Assert.Equal("�� ��� ������� �������� ��������� � ������������� ���������� - [1, 50].", exception.Message);
        }

        [Fact]
        public async Task CalculateSum_IncorrectMinValue_ThrowArgumentException()
        {
            var values = new List<double> { 0 };

            var service = new CalculationService(_settingsMock.Object, _cache);

            var exception = await Assert.ThrowsAsync<ArgumentException>(() => service.CalculateSum(values));

            Assert.NotNull(exception);
            Assert.Equal("�� ��� ������� �������� ��������� � ������������� ���������� - [1, 50].", exception.Message);
        }

        [Fact]
        public async Task CalculatePow_CorrectArguments_ReturnCorrectResult()
        {
            var service = new CalculationService(_settingsMock.Object, _cache);

            var result = await service.CalculatePow(5);

            Assert.Equal(25, result);
        }
    }
}