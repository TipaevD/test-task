﻿using System.Collections.Generic;

namespace TestTask.Main.Domain
{
    /// <summary>
    /// Запрос.
    /// </summary>
    public class Request
    {
        /// <summary>
        /// Входящие значения.
        /// </summary>
        public List<double> Values { get; set; }
    }
}
