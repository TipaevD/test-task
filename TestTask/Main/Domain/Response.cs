﻿namespace TestTask.Main.Domain
{
    /// <summary>
    /// Ответ на запрос.
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Результат запроса.
        /// </summary>
        public double Result { get; set; }
    }
}
