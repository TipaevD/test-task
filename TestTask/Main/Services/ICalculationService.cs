﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestTask.Main.Services
{
    public interface ICalculationService
    {
        Task<double> CalculateSum(List<double> values);

        Task<double> CalculatePow(double value);
    }
}
