﻿namespace TestTask.Main.Services
{
    /// <summary>
    /// Интерфейс настроек ограничений.
    /// </summary>
    public interface ICalculationSettings
    {
        /// <summary>
        /// Минимально возможное значение.
        /// </summary>
        double MinimumValue { get; }

        /// <summary>
        /// Максимально возможное значение.
        /// </summary>
        double MaximumValue { get; }

        /// <summary>
        ///  Максимальное количество входящих значений.
        /// </summary>
        int Limit { get; }

        /// <summary>
        /// Минимальное время задержки.
        /// </summary>
        int MinDelayTime { get; }

        /// <summary>
        /// Максимальное время задержки.
        /// </summary>
        int MaxDelayTime { get; }
    }
}
