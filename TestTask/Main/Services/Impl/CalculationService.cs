﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestTask.Main.Services.Impl
{
    public class CalculationService : ICalculationService
    {
        private readonly ICalculationSettings _settings;
        private readonly IMemoryCache _cache;

        public CalculationService(ICalculationSettings settings, IMemoryCache cache)
        {
            _settings = settings;
            _cache = cache;
        }

        public async Task<double> CalculateSum(List<double> values)
        {
            if (values == null || values.Count == 0)
            {
                throw new ArgumentException("Входящий массив не имеет значений.");
            }

            if (values.Count > _settings.Limit)
            {
                throw new ArgumentException($"Количество входящих значений превышает установленный лимит - {_settings.Limit}.");
            }

            double result = 0;

            foreach (var value in values)
            {
                if (value > _settings.MaximumValue ||  value < _settings.MinimumValue)
                {
                    throw new ArgumentException($"Не все входные значения находятся в установленном промежутке - [{_settings.MinimumValue}, {_settings.MaximumValue}].");
                }

                if (_cache.TryGetValue(value.ToString(), out double cachedValue))
                {
                    result += cachedValue;
                }
                else
                {
                    result += await CalculatePow(value);
                }
            }

            return result;
        }

        public async Task<double> CalculatePow(double value)
        {
            var random = new Random();

            await Task.Delay(random.Next(_settings.MinDelayTime, _settings.MaxDelayTime));

            var result = Math.Pow(value, 2);
            _cache.Set(value.ToString(), result);

            return result;
        }
    }
}
