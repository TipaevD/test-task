﻿using Microsoft.Extensions.Configuration;

namespace TestTask.Main.Services.Impl
{
    /// <summary>
    /// Настройки ограничений.
    /// </summary>
    public class CalculationSettings : ICalculationSettings
    {
        public double MinimumValue { get; }

        public double MaximumValue { get; }

        public int Limit { get; }

        public int MinDelayTime { get; }

        public int MaxDelayTime { get; }

        public CalculationSettings(IConfiguration configuration)
        {
            MinimumValue = configuration.GetValue("MinimumValue", 1.0);
            MaximumValue = configuration.GetValue("MaximumValue", 50.0);
            Limit = configuration.GetValue("Limit", 20);
            MinDelayTime = configuration.GetValue("MinDelayTime", 100);
            MaxDelayTime = configuration.GetValue("MaxDelayTime", 1000);
        }
    }
}
