﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TestTask.Main.Domain;
using TestTask.Main.Services;

namespace TestTask.Main.Controllers
{
    [ApiController]
    [Route("calculation")]
    public class CalculationController : ControllerBase
    {
        private readonly ICalculationService _calculationService;

        public CalculationController(ICalculationService calculationService)
        {
            _calculationService = calculationService;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Calculate([FromBody] Request request)
        {
            try
            {
                var result = await _calculationService.CalculateSum(request.Values);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
